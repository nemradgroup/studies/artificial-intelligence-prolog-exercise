%%% Input facts %%%
%%% Facts that are changed according to the map in the input.
%% paczka/4 %%
%% Very similar to box/4 but in Polish and in a strange form - (char)y, x, width, height. Part of interface but used as input.
:- dynamic paczka/4.

paczka(a,1,2,2).
paczka(e,6,2,2).
paczka(d,1,2,3).
paczka(a,3,2,1).
paczka(b,4,2,1).
paczka(f,4,2,1).
paczka(c,4,1,2).
paczka(a,7,1,2).
paczka(c,7,1,2).
paczka(c,5,1,1).
paczka(d,5,1,1).
paczka(a,6,1,1).

%% box/4 %%
%% Represents a single box on the board, with top-left as anchor point and x, y, width and height (in such an order) as parameters.
box(X, Y, Width, Height) :-
  paczka(AlphaY, X, Width, Height),
  char_code(AlphaY, Y).

%%% Core rules %%%
%%% Rules that define basic core rules needed for proper movement and positioning.
%% field_taken/2 %%
%% Checks if specified field (X, Y) is taken or not.
field_taken(X, Y) :-
  box(BoxX, BoxY, Width, Height), % there is some box
  X >= BoxX,
  Width > X-BoxX,
  Y >= BoxY,
  Height > Y-BoxY, !.

%% single_point_move_possible/4 %%
%% Checks if something can theoretically be placed on (X0, Y0) and be moved up to (XE, YE), that is all field on this route are free.
%% It's the most standard move, considering only a single point, moving on straight path.
% variant checking whether target has been reached
single_point_move_possible(X, Y, X, Y) :-
  part_of_board(X,Y),
  \+ field_taken(X, Y), !.
% variant for handling vertical movement
single_point_move_possible(X, Y0, X, YE) :-
  part_of_board(X, YE),
  \+ field_taken(X, Y0),
  (
    ( % move up
      Y0 > YE,
      MidY is Y0-1,
      single_point_move_possible(X, MidY, X, YE)
    );
    ( % move down
      YE > Y0,
      MidY is Y0+1,
      single_point_move_possible(X, MidY, X, YE)
    )
  ), !.
% variant for handling horizontal movement
single_point_move_possible(X0, Y, XE, Y) :-
  part_of_board(XE, Y),
  \+ field_taken(X0, Y),
  (
    ( % move left
      X0 > XE,
      MidX is X0-1,
      single_point_move_possible(MidX, Y, XE, Y)
    );
    ( % move right
      XE > X0,
      MidX is X0+1,
      single_point_move_possible(MidX, Y, XE, Y)
    )
  ), !.

%% single_box_move_possible/6 %%
%% Checks whether moving a box (a rectangular collection of points) in one dimension continuously is possible (there are no barriers on the path).
% stop condition variants
single_box_move_possible(X, _, 0, _, X, _) :- !.
single_box_move_possible(_, Y, _, 0, _, Y) :- !.
% vertical movement variant
single_box_move_possible(X, Y0, Width, Height, X, YE) :-
  (
    ( % move up
      Y0 > YE,
      RealY0 is Y0-1,
      RealYE is YE
    );
    ( % move down
      YE > Y0,
      RealY0 is Y0+Height,
      RealYE is YE+Height-1
    )
  ),
  CutWidth is Width-1, % repeat that on whole width
  RealX is X+CutWidth,
  single_point_move_possible(RealX, RealY0, RealX, RealYE),
  single_box_move_possible(X, Y0, CutWidth, Height, X, YE), !.
% horizontal movement variant
single_box_move_possible(X0, Y, Width, Height, XE, Y) :-
  (
    ( % move left
      X0 > XE,
      RealX0 is X0-1,
      RealXE is XE
    );
    ( % move right
      XE > X0,
      RealX0 is X0+Width,
      RealXE is XE+Width-1
    )
  ),
  CutHeight is Height-1, % repeat that on whole height
  RealY is Y + CutHeight,
  single_point_move_possible(RealX0, RealY, RealXE, RealY),
  single_box_move_possible(X0, Y, Width, CutHeight, XE, Y), !.

%% part_of_board/2 %%
%% Checks whether given point is on the board (X must be between 1 and 7, Y must be between a and f wriiten in ascii codes).
part_of_board(X, Y) :-
  between(1, 7, X),
  between(97, 102, Y).

%%% Compound rules %%%
%%% Rules that allow finding solutions for proper problems.
%% remove_box/2 %%
%% Simply removes a box from the board.
remove_box(X, Y, Width, Height) :-
  char_code(AlphaY, Y),
  retract(paczka(AlphaY, X, Width, Height)).

%% add_box/2 %%
%% Simply adds a box to the board.
add_box(X, Y, Width, Height) :-
  char_code(AlphaY, Y),
  assertz(paczka(AlphaY, X, Width, Height)).

%% move_box/4 %%
%% Moves a box at X0, Y0 to position XE, YE. Mutable!
move_box(X0, Y0, XE, YE) :-
  box(X0, Y0, Width, Height),
  remove_box(X0, Y0, Width, Height),
  add_box(XE, YE, Width, Height), !.

%% linear_box_move_possible/4 %%
%% Checks whether a destinations in form of a XE, YE tuple for a single move for a box anchored at X0, Y0 is possible. Similar to
%% single_box_move_possible/6, except it handles resettling boxes and is aware of box to move.
linear_box_move_possible(X0, Y0, XE, YE) :-
  box(X0, Y0, Width, Height),
  remove_box(X0, Y0, Width, Height),
  ( % it doesn't matter if the move was impossible to find, it should return the box to the original position regardless
    (
      single_box_move_possible(X0, Y0, Width, Height, XE, YE),
      T is 1
    );
    (
      T is 0
    )
  ),
  add_box(X0, Y0, Width, Height),
  (
    (
      T = 1
    );
    (
      false
    )
  ), !.

%% nonlinear_box_move_path/5 %%
%% Finds all (if any) paths for all possible nonlinear box moves (the ones involving multiple linear moves of the box in question) for a box
%% anchored at X0, Y0. They end at XE, YE and the paths are in form of lists of X, Y tuples (start positions for all smaller moves + end position).
nonlinear_box_move_path(X0, Y0, XE, YE, CurrentPath, FinalPath) :-
  length(CurrentPath, CurrentPathLength),
  CurrentPathLength < 3, % kill switch to optimize
  part_of_board(X, Y),
  [X, Y] \= [X0, Y0],
  \+ member([X, Y], CurrentPath),
  linear_box_move_possible(X0, Y0, X, Y),
  move_box(X0, Y0, X, Y),
  (
    (
      [X, Y] = [XE, YE],
      append(CurrentPath, [[X0, Y0], [XE, YE]], FinalPath),
      move_box(X, Y, X0, Y0)
    );
    (
      [X, Y] \= [XE, YE],
      append(CurrentPath, [[X0, Y0]], IntermediatePath),
      ( % it doesn't matter if the move was impossible to find, it should return the box to the original position regardless
        (
          nonlinear_box_move_path(X, Y, XE, YE, IntermediatePath, FinalPath),
          T is 1
        );
        (
          T is 0
        )
      ),
      move_box(X, Y, X0, Y0),
      (
        (
          T = 1
        );
        (
          false
        )
      )
    )
  ).

%% multistage_box_move_sequence/6 %%
%% Finds all (if any) sequences of nonlinear moves of boxes that would allow moving a box anchored at X0, Y0 to XE, YE.
%% Sequence is in form of a list of paths for nonlinear moves of boxes. Nonmovable list contains (X, Y) tuples of
%% anchored boxes that cannot be moved further (they have been already moved).
multistage_box_move_sequence(X0, Y0, X0, Y0, CurrentSequence, CurrentSequence, _).
multistage_box_move_sequence(X0, Y0, XE, YE, CurrentSequence, FinalSequence, Nonmovable) :-
  length(CurrentSequence, CurrentSequenceLength),
  CurrentSequenceLength < 1, % kill switch to optimize - too much, it should be regulated
  box(XBox, YBox, _, _), % select some box
  \+ member([XBox, YBox], Nonmovable), % ...which is movable (hasn't been moved yet in that multistage move)
  part_of_board(XDest, YDest), % select some destination point on the board
  nonlinear_box_move_path(XBox, YBox, XDest, YDest, [], Path), % check if the move is possible
  append(CurrentSequence, [Path], NewSequence), % update sequence
  move_box(XBox, YBox, XDest, YDest), % move the random box
  append(Nonmovable, [[XDest, YDest]], NewNonmovable), % disable further movements for that box
  ( % use recursion but dont quit immediately in case of failure
    (
      ( % recursively call other moves
        (
          [XBox, YBox] = [X0, Y0],
          multistage_box_move_sequence(XDest, YDest, XE, YE, NewSequence, FinalSequence, NewNonmovable)
        );
        (
          [XBox, YBox] \= [X0, Y0],
          multistage_box_move_sequence(X0, Y0, XE, YE, NewSequence, FinalSequence, NewNonmovable)
        )
      ),
      T is 1
    );
    (
      T is 0
    )
  ),
  move_box(XDest, YDest, XBox, YBox),
  (
    (
      T = 1
    );
    (
      false
    )
  ).

%%% Interface %%%
%%% Rules used to connect internal predicates with predicates specified in the instruction.
%% przem/4 %%
%% Checks whether some box anchored at X0, Y0 can be moved nonlinearly to XE, YE. Finds possible XE, YE if they're not specified.
przem(AlphaY0, X0, AlphaYE, XE) :-
  char_code(AlphaY0, Y0),
  part_of_board(XE, YE),
  nonlinear_box_move_path(X0, Y0, XE, YE, [], _),
  char_code(AlphaYE, YE).
